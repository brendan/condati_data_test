# Alembic Commands
A simple reminder of alembic commands

## Create Revision
```
alembic revision --autogenerate -m "<text>"
```

## Run Migration
```
alembic upgrade head
```


