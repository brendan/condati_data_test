FROM python:3.6-slim
LABEL maintainer="cupjohn"

ADD . /test_basis
WORKDIR /test_basis

RUN apt-get update  && apt-get install -y build-essential

RUN pip install pipenv
RUN pipenv install --deploy --system
