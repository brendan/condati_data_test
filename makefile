SHELL:=/bin/bash

test:
	PYTHONPATH=. pytest -v

isort:
	isort -rc . .isort.cfg



dag_source_dir = ./humans

lint:
	isort -c -rc . .isort.cfg
	flake8 --max-line-length=165 --ignore=E402 .
	pylint $(dag_source_dir) -r n

test-cov:
	PYTHONPATH=./humans pytest --cov=humans ./tests
