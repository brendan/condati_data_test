from humans import models
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
import pytest
from alembic.command import upgrade as alembic_upgrade
from alembic.config import Config as AlembicConfig

db = {
    'host': 'localhost',
    'port': '5432',
    'user': '',
    'password': '',
    'dbname': 'delete_me'
}
url = 'postgresql://{user}:{password}@{host}:{port}/{dbname}'.format(**db)


@pytest.fixture(scope='session')
def db():
    engine = create_engine(url, echo=True)
    session_factory = sessionmaker(bind=engine)
    print('\n----- CREATE TEST DB CONNECTION POOL\n')

    _db = {
        'engine': engine,
        'session_factory': session_factory,
    }
    alembic_config = AlembicConfig('/Users/charlesupjohn/projects/condati_data_test/alembic.ini')
    alembic_config.set_main_option('sqlalchemy.url', url)
    alembic_upgrade(alembic_config, 'head')
    print('\n----- RUN ALEMBIC MIGRATION\n')
    yield _db
    print('\n----- CREATE TEST DB INSTANCE POOL\n')

    engine.dispose()
    print('\n----- RELEASE TEST DB CONNECTION POOL\n')


@pytest.fixture(scope='function')
def session(db):
    session = db['session_factory']()
    yield session
    print('\n----- CREATE DB SESSION\n')

    session.rollback()
    session.close()
    print('\n----- ROLLBACK DB SESSION\n')


def test_thing(session):
    thing = models.Human.seed(session)
